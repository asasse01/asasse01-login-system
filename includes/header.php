<?php session_start(); ?>
<?php include 'includes/menu.php'; ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Welcome to Sassela - <?php echo $menu[$titleurl]; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <h1>Welcome to Sassela</h1>
	<div id="page">
        
        <?php 
        if (isset($_SESSION['username']))   {
            echo '<p>';
            echo 'Hello '.$_SESSION['username'].'. You are currently logged in.';
            include 'includes/formlogout.php';
            echo '</p>';
        } elseif (strpos($_SERVER['REQUEST_URI'], 'register.php')) {
            echo '';
        } else {

            include 'includes/formlogin.php';
        }

        printmenu($menu); 
        ?>
    