<?php 
ob_start();
$self = htmlentities($_SERVER['PHP_SELF']); 
?> 


<?php
/*
=====================LOGIN INPUT VALIDATION========================
*/
// Creates variables to store data/errors
$clean = array();
$input_errors = array();

// Sets state flags
$form_is_submitted = false;
$input_errors_detected = false;
$login_status = false;

// Checks for submitted form
if (!empty($_POST['login_submit'])) {
        
    $form_is_submitted = true;

    // Validates the Username entry
    $makeHtml = trim($_POST['username']);
    if(!empty($_POST['username'])) {
        if (strlen($makeHtml)<=30) {
            $clean['username'] = htmlentities($makeHtml);
        }
        else {
            $input_errors['username'] = 'Username not valid, please try again.';
            $input_errors_detected = true;
        }
    }
    else {
        $input_errors['username'] = 'Please enter your username.';
        $input_errors_detected = true;
    }

    // Validates the Password entry
    $makeHtml = $_POST['password'];
    if(!empty($_POST['password'])) {
        if (strlen($makeHtml)<=150) {
            $clean['password'] = htmlentities($makeHtml);
        }
        else {
            $input_errors['password'] = 'Password too long, please try again.';
            $input_errors_detected = true;
        }
    }
    else {
        $input_errors['password'] = 'Please enter your password.';
        $input_errors_detected = true;
    }

}
?>

<?php
/*
=====================LOGIN STORAGE / ERROR REDISPLAY========================
*/
?>

<form action="<?php echo $self; ?>" method="post">
    <fieldset>
    <legend>Log in</legend>
    <?php

        if ($form_is_submitted === true && $input_errors_detected !== false) {
            //diplays errors
            echo '<p>Please correct the following errors:</p>';
            foreach ($input_errors as $error) {
                echo '<p>'.$error.'</p>';
            }      
        } elseif ($form_is_submitted === true && $input_errors_detected === false) {
            // If the form was submitted & data valid, process data
            $processing_errors = array();
            $processing_errors_detected = false;

            $username = $clean['username'];
            $password = $clean['password'];

            $handle = fopen('includes/details.php', 'r');

            while(!feof($handle)) {
                $line[] = fgets($handle);

                foreach($line as $details_string) {
                    $user_details_all[] = unserialize($details_string);
                }

                foreach($user_details_all as $user) {
                //print_r($user);
                    if($username == $user['username']) {
                        if($password != $user['password']) {
                            $processing_errors['input_wrong'] = 'That password isn\'t right. Please try again or <a href=register.php?'.SID.'>register</a>';
                            $processing_errors_detected = true;
                            break;
                        } else {
                            $processing_errors_detected = false;
                            break;
                        }
                    } else {
                        $processing_errors['input_wrong'] = 'That username doesn\'t exist. Please try again or <a href=register.php?'.SID.'>register</a>';
                        $processing_errors_detected = true;
                    }
                }
            }
            fclose($handle);

            if ($form_is_submitted === true && $processing_errors_detected !== false) {
                echo '<p>There were following errors:</p>';
                foreach ($processing_errors as $error) {
                    echo '<p>'.$error.'</p>';
                }      
            } elseif ($form_is_submitted === true && $processing_errors_detected === false) {
                foreach ($clean as $key => $details) {
                    $_SESSION[$key] = $details;
                }
                $login_status = true;
                header('Location: http://titan.dcs.bbk.ac.uk/~asasse01/p1fma/index.php?'.SID.'');
            }
        } 



    //tests whether username entry is valid & in the event of password error keeps entry
    if (isset($clean['username'])) {
        $username = htmlentities($clean['username']); 
    } else { 
        $username = ''; 
    } 

    ?> 
        <div>
            <label for="username">Username</label>
            <input value="<?php echo $username; ?>" type="text" name="username" id="username" />
        </div>
        <div>            
            <label for="password">Password</label>
            <input type="password" name="password" id="password" />
        </div>
        <div>            
            <input type="submit" name="login_submit" value="submit" />
        </div>
        <p>Not signed up? <a href ="register.php?<?php echo SID; ?>">Register here</a>.</p> 
    </fieldset>
</form>