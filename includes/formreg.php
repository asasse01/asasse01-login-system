<?php 
ob_start();
$self = htmlentities($_SERVER['PHP_SELF']); 
?> 

<?php
/*
=====================REGISTRATION FORM VALIDATION========================
*/
// Creates variables to store data/errors
$clean = array();
$input_errors = array();

// Sets state flags
$form_is_submitted = false;
$input_errors_detected = false;
$reg_status = false;

// Checks for submitted form
if (!empty($_POST['submit'])) {
        
    $form_is_submitted = true;

    // Validates the Full Name entry
    $makeHtml = trim($_POST['name']);
    if(!empty($_POST['name'])) {

        if (strlen($makeHtml)<=150 ) {

            $clean['name'] = htmlentities($makeHtml);
        }
        else {
            $input_errors['name'] = 'Please enter your full name using fewer than 150 characters.';
            $input_errors_detected = true;
        }
    }
    else {
        $input_errors['name'] = 'Please enter your full name.';
        $input_errors_detected = true;
    }

    // Validates the Username entry
    $makeHtml = trim($_POST['username']);
    if(!empty($_POST['username'])) {
        if (strlen($makeHtml)<=30) {
            $clean['username'] = htmlentities($makeHtml);
        }
        else {
            $input_errors['username'] = 'Please enter a username that is less than 30 characters.';
            $input_errors_detected = true;
        }
    }
    else {
        $input_errors['username'] = 'Please enter your username.';
        $input_errors_detected = true;
    }

    // Validates the Password entry
    $makeHtml = $_POST['password'];
    if(!empty($_POST['password'])) {
        if (strlen($makeHtml)<=150) {
            $clean['password'] = htmlentities($makeHtml);
        }
        else {
            $input_errors['password'] = 'Please enter a password that is less than 150 characters.';
            $input_errors_detected = true;
        }
    }
    else {
        $input_errors['password'] = 'Please enter your password.';
        $input_errors_detected = true;
    }
        
    // Validates the Email entry    
    $makeHtml = trim($_POST['email']);
    if(!empty($_POST['email'])) {
        if (filter_var($makeHtml,FILTER_VALIDATE_EMAIL)) {
            $clean['email'] = htmlentities($makeHtml);
        }
        else {
            $input_errors['email'] = 'Please enter your email address in the format someone@example.com.';
            $input_errors_detected = true;
        }
    }
    else {
        $input_errors['email'] = 'Please enter your email.';
        $input_errors_detected = true;
    }

}   

?>

<!--
===================== PROCESS DATA ========================
-->

<form action="<?php echo $self; ?>" method="post">
    <fieldset>
    <legend>Register</legend>
    <?php
       
    if ($form_is_submitted === true && $input_errors_detected !== false) {
        //diplays errors
        echo '<p>Please correct the following errors:</p>';
        foreach ($input_errors as $error) {
            echo '<p>'.$error.'</p>';
        }      
    } elseif ($form_is_submitted === true && $input_errors_detected === false) {
        // If the form was submitted & data valid, process data
        $user = array();
        $processing_errors = array();
        $processing_errors_detected = false;

        $username = $clean['username'];
        $email = $clean['email'];

        $handle = fopen('includes/details.php', 'r');
        while(!feof($handle)) {
            $line[] = fgets($handle);
            foreach($line as $details_string) {
                $user_details_all[] = unserialize($details_string);
            }

            foreach($user_details_all as $user) {
            //print_r($user);
                if($username == $user['username']) {
                    $processing_errors_detected = true;
                    $processing_errors['usr_taken'] = 'Sorry, that username is already taken. Please try another one or log in.';
                    break;
                }
            }

            foreach($user_details_all as $user) {
            //print_r($user);
                if($email == $user['email']) {
                    $processing_errors_detected = true;
                    $processing_errors['email_taken'] = 'Sorry, that email address is already taken. Please try another one or log in.';
                    break;
                }
            }
        }
        fclose($handle);

        if ($form_is_submitted === true && $processing_errors_detected !== false) {
            echo '<p>There were following errors:</p>';
            foreach ($processing_errors as $error) {
                echo '<p>'.$error.'</p>';
            }
        } elseif ($form_is_submitted === true && $processing_errors_detected === false) {
            //stores serialized session array details into details.php
            $user = $clean;
            //sets username as the key for stored user data
            $details_string = serialize($user).PHP_EOL;
            $result = file_put_contents('includes/details.php', $details_string, FILE_APPEND);
            if ($result === false) {
                echo 'There was an error storing your details, please try again.';
            } else {
                foreach ($clean as $key => $details) {
                    $_SESSION[$key] = $details;
                }
                $reg_status = true;
                header('Location: http://titan.dcs.bbk.ac.uk/~asasse01/p1fma/index.php?'.SID.'');
            }
        }
    }       
    ?>

    <?php 
    //tests whether name, email & username entries are valid & in the event of errors keeps any correct entries
    if (!empty($clean['name'])) {
        $name = htmlentities($clean['name']); 
    } else { 
        $name = ''; 
    } 

    if (!empty($clean['email'])) {
        $email = htmlentities($clean['email']); 
    } else { 
        $email = ''; 
    } 

    if (!empty($clean['username'])) {
        $username = htmlentities($clean['username']); 
    } else { 
        $username = ''; 
    } 

    ?> 
      
        <div>            
            <label for="name">Full Name</label>
            <input value="<?php echo $name; ?>" type="text" name="name" id="name" />
        </div>
        <div>            
            <label for="email">Email</label>
            <input value="<?php echo $email; ?>" type="text" name="email" id="email" />
        </div>
        <div>
            <label for="username">Username</label>
            <input value="<?php echo $username; ?>" type="text" name="username" id="username" />
        </div>
        <div>            
            <label for="password">Password</label>
            <input type="password" name="password" id="password" />
        </div>
        <div>            
            <input type="submit" name="submit" value="submit" />
        </div>
    </fieldset>
</form>