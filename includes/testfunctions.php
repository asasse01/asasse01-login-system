<?php 
$self = htmlentities($_SERVER['PHP_SELF']); 
?> 

<?php
/*
=====================REGISTRATION FORM VALIDATION========================
*/


    // Creates variables to store data/errors
    $clean = array();
    $errors = array();

    // Sets state flags
    $form_is_submitted = false;
    $errors_detected = false;
    $reg_status = false;

    // Checks for submitted form
    if (isset($_POST['submit'])) {
            
        $form_is_submitted = true;

function validate_reg($_POST) {
/* 
function validates registration details from $_POST data
Parameters: input to be validated (assumed name, username, email & password)
Returns: boolean sumbit & valid?
*/
        // Validates the Full Name entry
        $makeHtml = trim($_POST['name']);
        if(isset($_POST['name'])) {

            if (strlen($makeHtml)<=150 ) {

                $clean['name'] = htmlentities($makeHtml);
            }
            else {
                $errors['name'] = 'Name not valid, please try again.';
                $errors_detected = true;
            }
        }
        else {
            $errors['name'] = 'Please enter your full name.';
            $errors_detected = true;
        }

        // Validates the Username entry
        $makeHtml = trim($_POST['username']);
        if(isset($_POST['username'])) {
            if (strlen($makeHtml)<=30) {
                $clean['username'] = htmlentities($makeHtml);
            }
            else {
                $errors['username'] = 'Username not valid, please try again.';
                $errors_detected = true;
            }
        }
        else {
            $errors['username'] = 'Please enter your username.';
            $errors_detected = true;
        }

        // Validates the Password entry
        $makeHtml = $_POST['password'];
        if(isset($_POST['password'])) {
            if (strlen($makeHtml)<=150) {
                $clean['password'] = htmlentities($makeHtml);
            }
            else {
                $errors['password'] = 'Password too long, please try again.';
                $errors_detected = true;
            }
        }
        else {
            echo '<p>Please enter your password.</p>';
            $errors['password'] = 'Please enter your password.';
            $errors_detected = true;
        }
            
        // Validates the Email entry    
        $makeHtml = trim($_POST['email']);
        if(isset($_POST['email'])) {
            if (filter_var($makeHtml,FILTER_VALIDATE_EMAIL)) {
                $clean['email'] = htmlentities($makeHtml);
            }
            else {
                $errors['email'] = 'Email: Not valid, please try again.';
                $errors_detected = true;
            }
        }
        else {
            $errors['email'] = 'Please enter your email.';
            $errors_detected = true;
        }

/*
---------TESTING FOR USR ALREADY CREATED (DUPLICATE_REG)--------
 */
/* 
function checks serialized reg details file for duplicate username or email registration attempt
Parameters: username & email to be checked, file to be searched
Returns: errors array
*/
function duplicate_reg($username, $email, $file) {
    $handle = fopen($file, 'r');

    $details_string = file_get_contents($handle);
    $user = unserialize($details_string);

    if(in_array($username, $handle)) {
        $errors_detected = true;
        $errors['usr_taken'] = 'Sorry, that username is already in use. Please try another one or log in.';
    }

    if(in_array($email, $handle)) {
        $errors_detected = true;
        $errors['email_taken'] = 'Sorry, that email address is already in use. Please try another one or log in.';
    }

    fclose($handle);

    return($errors);
}

    /*
    ---------END TESTING------------------
    */

    }   

    /*else {
        $errors['reg'] = 'Registration form not submitted correctly, please try again';
        $errors_detected = true;
    }*/

}
?>

<!--
===================== REGISTRATION FORM DISPLAYING / REDISPLAYING W ERRORS / DETAILS STORAGE ========================
-->

<form action="<?php echo $self; ?>" method="post">
    <fieldset>
    <legend>Register</legend>

<!--
===================== DETAILS STORAGE / ERROR DISPLAY ========================
-->
    <?php

    $user = array();
    // If the form was submitted & data valid
    if ($form_is_submitted === true && $errors_detected === false) {

        //stores serialized session array details into details.php
        /* 
        function appends serialized session array details into file 
        Parameters: array to be stored (username element assumed), file to store
        Returns: boolean result
        */
        function check_duplicate_reg($arrat, $file) {
        $username = $array['username'];
        $user[$username] = $array;
        $details_string = serialize($user).PHP_EOL;
        $result = file_put_contents($file, $details_string, FILE_APPEND);

        return $result;
        }

        $result = check_duplicate_reg($clean, 'includes/details.php');

        if ($result($array, $file) === false) {
            echo '<p>There was an error storing your details, please try again</p>';
            //$errors['storage'] = 'There was an error storing your details, please try again';
            //$errors_detected = true;
        } else {
            //store clean details into session array
            foreach ($clean as $key => $details) {
            $_SESSION[$key] = $details;
            }

            $reg_status = true;
            header('Location: http://titan.dcs.bbk.ac.uk/~asasse01/p1fma/index.php?'.SID.'');
        }

    } 
    //diplays errors
    elseif ($form_is_submitted === true && $errors_detected === true) {
        echo '<p>There were the following errors:</p>';
        foreach ($errors as $error) {
            echo '<p>'.$error.'</p>';
        }      
    }
    ?>

<!--
===================== REDISPLAYING FORM ========================
-->

    <?php 

    /* 
    function tests whether name, email & username entries are valid & in the event of errors keeps any correct entries 
    Parameters: array to be stored (username assumed), file
    Returns: clean array
    */
    function check_duplicate_reg($array, $file) {
    //tests whether name, email & username entries are valid & in the event of errors keeps any correct entries
        if (isset($clean['name'])) {
            $name = htmlentities($clean['name']); 
        } else { 
            $name = ''; 
        } 

        if (isset($clean['email'])) {
            $email = htmlentities($clean['email']); 
        } else { 
            $email = ''; 
        } 

        if (isset($clean['username'])) {
            $username = htmlentities($clean['username']); 
        } else { 
            $username = ''; 
        } 

        return $clean;

    }

    check_duplicate_reg($_POST, 'includes/details.php');

    ?> 
<!--
===================== HTML FORM ========================
-->
      
        <div>            
            <label for="name">Full Name</label>
            <input value="<?php echo $name; ?>" type="text" name="name" id="name" />
        </div>
        <div>            
            <label for="email">Email</label>
            <input value="<?php echo $email; ?>" type="text" name="email" id="email" />
        </div>
        <div>
            <label for="username">Username</label>
            <input value="<?php echo $username; ?>" type="text" name="username" id="username" />
        </div>
        <div>            
            <label for="password">Password</label>
            <input type="password" name="password" id="password" />
        </div>
        <div>            
            <input type="submit" name="submit" value="submit" />
        </div>
    </fieldset>
</form>