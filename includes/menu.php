<?php

// create menu array
$menu = array();
$menu['index.php?'.SID.''] = 'Home';
$menu['basket.php?'.SID.''] = 'My Basket';
$menu['account.php?'.SID.''] = 'My Account';
if (!isset($_SESSION['username'])) {    
    //if user is not already logged in, show registration page
    $menu['register.php?'.SID.''] = 'Register';
}

function printmenu($menu) {
	echo '|';
	foreach($menu as $key => $value) {  
	    echo ' <a href='.$key.'>'.$value.'</a>';
	    echo ' |';
	}
}

// defines title URL for use in header title
if (strpos($_SERVER['REQUEST_URI'], 'ccount.php')) {
	$titleurl  = 'account.php?'.SID.'';
} else {
	$titleurl = (ltrim($_SERVER['REQUEST_URI'], "/~asasse01/p1fma/")).SID.'';
}

?>