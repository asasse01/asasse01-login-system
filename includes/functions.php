<?php

/*
=====================REGISTRATION FORM VALIDATION========================
*/
// Creates variables to store data/errors
$clean = array();
$errors = array();

// Sets state flags
$form_is_submitted = false;
$errors_detected = false;
$reg_status = false;

// Checks for submitted form
if (isset($_POST['submit'])) {
        
    $form_is_submitted = true;

    // Validates the Full Name entry
    $makeHtml = trim($_POST['name']);
    if(isset($_POST['name'])) {

        if (strlen($makeHtml)<=150 ) {

            $clean['name'] = htmlentities($makeHtml);
        }
        else {
            $errors['name'] = 'Name not valid, please try again.';
            $errors_detected = true;
        }
    }
    else {
        $errors['name'] = 'Please enter your full name.';
        $errors_detected = true;
    }

    // Validates the Username entry
    $makeHtml = trim($_POST['username']);
    if(isset($_POST['username'])) {
        if (strlen($makeHtml)<=30) {
            $clean['username'] = htmlentities($makeHtml);
        }
        else {
            $errors['username'] = 'Username not valid, please try again.';
            $errors_detected = true;
        }
    }
    else {
        $errors['username'] = 'Please enter your username.';
        $errors_detected = true;
    }

    // Validates the Password entry
    $makeHtml = $_POST['password'];
    if(isset($_POST['password'])) {
        if (strlen($makeHtml)<=150) {
            $clean['password'] = htmlentities($makeHtml);
        }
        else {
            $errors['password'] = 'Password too long, please try again.';
            $errors_detected = true;
        }
    }
    else {
        echo '<p>Please enter your password.</p>';
        $errors['password'] = 'Please enter your password.';
        $errors_detected = true;
    }
        
    // Validates the Email entry    
    $makeHtml = trim($_POST['email']);
    if(isset($_POST['email'])) {
        if (filter_var($makeHtml,FILTER_VALIDATE_EMAIL)) {
            $clean['email'] = htmlentities($makeHtml);
        }
        else {
            $errors['email'] = 'Email: Not valid, please try again.';
            $errors_detected = true;
        }
    }
    else {
        $errors['email'] = 'Please enter your email.';
        $errors_detected = true;
    }


    ?>