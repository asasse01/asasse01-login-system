<?php
ob_start();
//clear cookies & destroy session on logout
if(isset($_POST['logout']))	{

	$_SESSION = array();
	if (ini_get("session.use_cookies")) {
		$yesterday = time() - (24 * 60 * 60); $params = session_get_cookie_params(); setcookie(session_name(), '', $yesterday,
		$params["path"], $params["domain"],
		$params["secure"], $params["httponly"] );
	}
	session_destroy();
	header('Location: http://titan.dcs.bbk.ac.uk/~asasse01/p1fma/index.php');

}

?>