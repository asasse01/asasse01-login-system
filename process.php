<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Form processing page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <h1>Data from the form:</h1>

        <?php
        $makeHtml = $_POST['username'];
		if(isset($_POST['username']))
		{
			if (strlen($makeHtml)<=30) 
			{
				$username = htmlentities($makeHtml);
				$_SESSION['username'] = $username;
			}
			else
			{
				echo '<p>Username not valid, please try again.</p>';
			}

		}
		else
		{
			echo '<p>Username field empty, please try again.</p>';
		}

		$makeHtml = $_POST['password'];
		if(isset($_POST['password']))
		{
			if (strlen($makeHtml)<=150) 
			{
				$password = htmlentities($makeHtml);
				$_SESSION['password'] = $password;
			}
			else
			{
				echo '<p>Password too long, please try again.</p>';
			}

		}
		else
		{
			echo '<p>Password field empty, please try again.</p>';
		}
			

        ?>

    </body>
</html>
