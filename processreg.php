<?php
//session_start();
?>
<!--<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <title>Form processing page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <h1>Data from the form:</h1>-->

<?php
// Creates variables to store data/errors
$clean = array();
$errors = array();

// Sets state flags
$form_is_submitted = false;
$errors_detected = false;
$reg_status = false;

// Check for submitted form
if (isset($_POST['submit'])) {
    	
	$form_is_submitted = true;

	// Validates the Full Name entry
    $makeHtml = trim($_POST['name']);
	if(isset($_POST['name'])) {

		if (strlen($makeHtml)<=150 ) {

			$clean['name'] = htmlentities($makeHtml);
		}
		else {
			//echo '<p>Name not valid, please try again.</p>';
			$errors['name'] = 'Name not valid, please try again.';
   			$errors_detected = true;
		}
	}
	else {
		//echo '<p>Please enter your full name.</p>';
		$errors['name'] = 'Please enter your full name.';
   		$errors_detected = true;
	}

	// Validates the Username entry
    $makeHtml = trim($_POST['username']);
	if(isset($_POST['username'])) {
		if (strlen($makeHtml)<=30) {
			$clean['username'] = htmlentities($makeHtml);
		}
		else {
			//echo '<p>Username not valid, please try again.</p>';
			$errors['username'] = 'Username not valid, please try again.';
   			$errors_detected = true;
		}
	}
	else {
		//echo '<p>Please enter your username.</p>';
		$errors['username'] = 'Please enter your username.';
   		$errors_detected = true;
	}

	// Validates the Password entry
	$makeHtml = $_POST['password'];
	if(isset($_POST['password'])) {
		if (strlen($makeHtml)<=150) {
			$clean['password'] = htmlentities($makeHtml);
		}
		else {
			//echo '<p>Password too long, please try again.</p>';
			$errors['password'] = 'Password too long, please try again.';
   			$errors_detected = true;
		}
	}
	else {
		echo '<p>Please enter your password.</p>';
		$errors['password'] = 'Please enter your password.';
   		$errors_detected = true;
	}
		
	// Validates the Email entry	
	$makeHtml = trim($_POST['email']);
	if(isset($_POST['email'])) {
		if (filter_var($makeHtml,FILTER_VALIDATE_EMAIL)) {
			$clean['email'] = htmlentities($makeHtml);
		}
		else {
			//echo '<p>Email: Not valid, please try again.</p>';
			$errors['email'] = 'Email: Not valid, please try again.';
   			$errors_detected = true;
		}
	}
	else {
		//echo '<p>Please enter your email.</p>';
		$errors['email'] = 'Please enter your email.';
   		$errors_detected = true;
	}
}	
else {
    $errors['reg'] = 'Registration form not submitted';
    $errors_detected = true;
}

/*// If the form was submitted & data valid
if ($form_is_submitted === true && $errors_detected !== true) {
	//store clean details into session array
	foreach ($clean as $key => $details) {
		$_SESSION[$key] = $details;
	}

	//store clean details into details.txt file
	$towrite = fopen('http://titan.dcs.bbk.ac.uk/~asasse01/details.txt','w');
	fwrite($towrite,$_SESSION.PHP_EOL);
	fclose($towrite);   

	$reg_status = true;

	header('Location: http://titan.dcs.bbk.ac.uk/~asasse01/p1fma/index.php');

}
/*else {
	header('Location: http://titan.dcs.bbk.ac.uk/~asasse01/p1fma/register.php');
	/*echo '<p>There were the following errors:</p>';
	foreach ($errors as $error) {
		echo '<p>'.$error.'</p>';
	}
}*/



//print_r ($clean);
//print_r ($_SESSION);
?>
	        
    </body>
</html>
