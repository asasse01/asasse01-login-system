p1fma
=====
Your Task
A client has requested that you develop a generic login-system for their website. Your system should include the following aspects:
1. Log-in: A visitor should be able to log-in to the website by entering a valid username and password in a form. Accompanying the log-in form should be a link to allow the visitor to register as a new user if required.
2. Log-out: A logged in user will be able to log-out of the system, and a message will be displayed to this effect.
3. Register page: A page to register as a user if the visitor does not currently have a username and password. The data you require from a visitor to register as a user is their full name, email address, a username and a password.
4. Content pages: 3 content pages are required. A publicly viewable default index page, one other public content page, and one content page that is only available to logged in users. The user should be able to browse between pages while maintaining their logged-in or out status, regardless of their browser settings.
All pages should display a link or form to log-in or out depending on user state. If the user is logged in, the page should display the username of the logged in user. The content pages need not contain any actual content, but the area where this content would be added should be clearly indicated.
A user navigating to any of the public content pages should be able to view the content directly whether they are logged-in or logged-out. The pages should clearly display their user status. If a logged out user tries to access the private content page, they should be politely requested to log-in.
There is no need to apply any CSS or other formatting to your html output and such formatting is discouraged. However, the output should validate correctly under the DOCTYPE you have specified.
